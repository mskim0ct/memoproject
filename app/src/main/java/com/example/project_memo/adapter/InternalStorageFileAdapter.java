package com.example.project_memo.adapter;

import android.content.Context;

import com.example.project_memo.Util;
import com.example.project_memo.resource.Item;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class InternalStorageFileAdapter {

    private Context context;

    public InternalStorageFileAdapter(Context context){
        this.context = context;
    }

    public ArrayList<Item> getAll(){
        File path = context.getFilesDir();
        File[] files = path.listFiles();
        ArrayList<Item> itemArrayList = new ArrayList<>();
        for (File file : files){
            try {
                FileInputStream fis = new FileInputStream(file.getAbsolutePath());
                byte[] content = new byte[fis.available()];
                while(fis.read(content) != -1){}
                itemArrayList.add((Item)Util.convertToObject(new String(content), Item.class));
                fis.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return itemArrayList;
    }

    public void createFile(String fileName, Item item){
        try {
            FileOutputStream fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            byte[] bytes = Util.convertToJson(item).getBytes();
            fos.write(bytes);

            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteFile(String fileName){
        context.deleteFile(fileName);
    }

    public void updateFile(String fileName, Item item){
        deleteFile(fileName);
        createFile(fileName, item);
    }

    public Item get(String fileName){
        Item item = null;
        try {
            File file = new File(context.getFilesDir(), fileName);
            FileInputStream fis = new FileInputStream(file.getAbsolutePath());
            byte[] content = new byte[fis.available()];
            while( fis.read() != -1){}
            item = (Item)Util.convertToObject(new String(content), Item.class);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return item;
    }
}
