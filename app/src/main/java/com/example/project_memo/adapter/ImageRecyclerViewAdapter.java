package com.example.project_memo.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project_memo.CommonCode;
import com.example.project_memo.R;
import com.example.project_memo.UrlAsyncTask;
import com.example.project_memo.resource.Image;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ImageRecyclerViewAdapter extends RecyclerView.Adapter<ImageRecyclerViewAdapter.ImageViewHolder>{
    private ArrayList<Image> imageArrayList = new ArrayList<>();

    public class ImageViewHolder extends RecyclerView.ViewHolder{
        public ImageView imageView;
        public Context context;
        public ImageViewHolder(View view) {
            super(view);
            context = view.getContext();
            imageView = view.findViewById(R.id.image_view_id);
        }
    }

    @NonNull
    @Override
    public ImageRecyclerViewAdapter.ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_view_item, parent, false);
        return new ImageRecyclerViewAdapter.ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageRecyclerViewAdapter.ImageViewHolder holder, int position) {
        Image image = imageArrayList.get(position);
        if(image.getType() == CommonCode.TYPE_ALBUM){
            try {
                ParcelFileDescriptor pfd = holder.context.getContentResolver().openFileDescriptor(Uri.parse(image.getPath()), "r");
                Bitmap bitmap = BitmapFactory.decodeFileDescriptor(pfd.getFileDescriptor());
                holder.imageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else if(image.getType() == CommonCode.TYPE_CAMERA){
            Bitmap bitmap = BitmapFactory.decodeFile(image.getPath());
            holder.imageView.setImageBitmap(bitmap);
        }else if(image.getType() == CommonCode.TYPE_URL){
            UrlAsyncTask urlAsyncTask = new UrlAsyncTask(image.getPath(), holder.context);
            Bitmap bitmap = null;
            boolean isError = false;
            try {
                bitmap = urlAsyncTask.execute().get(3000, TimeUnit.MILLISECONDS);
            } catch (ExecutionException e) {
                e.printStackTrace();
                isError = true;
            } catch (InterruptedException e) {
                e.printStackTrace();
                isError = true;
            } catch (TimeoutException e) {
                e.printStackTrace();
                isError = true;
            }
            if(isError || bitmap == null){
                holder.imageView.setImageResource(R.drawable.ic_photo_black_24dp);
            }else{
                holder.imageView.setImageBitmap(bitmap);
            }
        }
    }

    @Override
    public int getItemCount() {
        return imageArrayList.size();
    }

    public void addImage(Image image){
        imageArrayList.add(image);
    }

    public ArrayList<Image> getAll(){
        return imageArrayList;
    }
}
