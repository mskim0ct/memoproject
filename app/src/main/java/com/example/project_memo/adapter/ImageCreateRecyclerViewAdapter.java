package com.example.project_memo.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project_memo.CommonCode;
import com.example.project_memo.R;
import com.example.project_memo.UrlAsyncTask;
import com.example.project_memo.resource.Image;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ImageCreateRecyclerViewAdapter extends RecyclerView.Adapter<ImageCreateRecyclerViewAdapter.ImageViewHolder>{

    private ArrayList<Image> imageArrayList = new ArrayList<>();
    private boolean CREATE = false;

    public class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener{
        public ImageView imageView;
        public Context context;
        public ImageViewHolder(View view) {
            super(view);
            context = view.getContext();
            imageView = view.findViewById(R.id.image_view_id);
            imageView.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(context.getString(R.string.info));
            builder.setMessage(context.getString(R.string.question_delete_image));
            builder.setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    int position = getAdapterPosition();
                    if(position != RecyclerView.NO_POSITION){
                        imageArrayList.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, imageArrayList.size());
                    }
                    dialog.dismiss();
                }
            });
            AlertDialog deleteAlert = builder.create();
            deleteAlert.show();
            return true;
        }
    }

    @NonNull
    @Override
    public ImageCreateRecyclerViewAdapter.ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_view_item, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageCreateRecyclerViewAdapter.ImageViewHolder holder, int position) {
        Image image = imageArrayList.get(position);
        if(image.getType() == CommonCode.TYPE_ALBUM){
            try {
                ParcelFileDescriptor pfd = holder.context.getContentResolver().openFileDescriptor(Uri.parse(image.getPath()), "r");
                Bitmap bitmap = BitmapFactory.decodeFileDescriptor(pfd.getFileDescriptor());
                holder.imageView.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else if(image.getType() == CommonCode.TYPE_CAMERA){
            Bitmap bitmap = BitmapFactory.decodeFile(image.getPath());
            holder.imageView.setImageBitmap(bitmap);
        }else if(image.getType() == CommonCode.TYPE_URL){
            if(CREATE){
                boolean isError = false;
                Bitmap bitmap = null;
                try {
                    UrlAsyncTask urlAsyncTask = new UrlAsyncTask(image.getPath(), holder.context);
                    bitmap = urlAsyncTask.execute().get(3000, TimeUnit.MILLISECONDS);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                    isError =true;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    isError =true;
                } catch (TimeoutException e) {
                    e.printStackTrace();
                    isError =true;
                }
                if(isError || bitmap == null){
                    imageArrayList.remove(position);
                    AlertDialog dialog = new AlertDialog.Builder(holder.context)
                            .setTitle(holder.context.getString(R.string.info))
                            .setMessage(holder.context.getString(R.string.cant_get_image))
                            .create();
                    dialog.show();
                }
                if(!isError && bitmap != null){
                    holder.imageView.setImageBitmap(bitmap);
                }
            }else{
                Bitmap bitmap = null;
                boolean isError = false;
                try {
                    UrlAsyncTask urlAsyncTask = new UrlAsyncTask(image.getPath(), holder.context);
                    bitmap = urlAsyncTask.execute().get(3000, TimeUnit.MILLISECONDS);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                    isError = true;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    isError = true;
                } catch (TimeoutException e) {
                    e.printStackTrace();
                    isError = true;
                }
                if(isError || bitmap == null){
                    holder.imageView.setImageResource(R.drawable.ic_photo_black_24dp);
                }else if(!isError && bitmap != null){
                    holder.imageView.setImageBitmap(bitmap);
                }
            }
            CREATE = false;
        }
    }

    @Override
    public int getItemCount() {
        return imageArrayList.size();
    }

    public void addImageFromURL(Image image){
        CREATE = true;
        imageArrayList.add(image);
    }
    public void addImage(Image image){
        imageArrayList.add(image);
    }


    public ArrayList<Image> getAll(){
        return imageArrayList;
    }


}
