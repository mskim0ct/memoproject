package com.example.project_memo.adapter;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.project_memo.CommonCode;
import com.example.project_memo.R;
import com.example.project_memo.UrlAsyncTask;
import com.example.project_memo.resource.Image;
import com.example.project_memo.resource.Item;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

//MainActivity 의 Memo 에 관한 listview adapter
public class ItemListViewAdapter extends BaseAdapter {

    ArrayList<Item> itemArrayList;

    public ItemListViewAdapter(){
        itemArrayList= new ArrayList<>();
    }

    public ArrayList<Item> getAll(){
        return itemArrayList;
    }

    public void add(Item item){
        itemArrayList.add(item);
    }

    public void delete(Item item){
        itemArrayList.remove(item);
    }

    public void delete(int position){
        itemArrayList.remove(position);
    }

    public void update(Item item, int position){
        itemArrayList.remove(position);
        itemArrayList.add(position, item);
    }

    @Override
    public int getCount() {
        return itemArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Item item = itemArrayList.get(position);
        if(convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.listview_memo_item, null);
        }

        //thumbnail
        ImageView imageView = convertView.findViewById(R.id.main_thumbnail_id);
        if(item.getImageArrayList() == null || item.getImageArrayList().size() == 0){
            //default 이미지
            imageView.setImageResource(R.drawable.ic_add_a_photo_black_24dp);
        }else{
            Bitmap bitmap = setImage(item.getImageArrayList().get(0), parent.getContext());
            if(bitmap == null){
                imageView.setImageResource(R.drawable.ic_photo_black_24dp);
            }else{
                imageView.setImageBitmap(bitmap);
            }
        }
        //title
        TextView titleTextView = convertView.findViewById(R.id.main_title_id);
        titleTextView.setText(item.getTitle());
        //content
        TextView contentTextView = convertView.findViewById(R.id.main_content_id);
        contentTextView.setText(item.getContent());
        return convertView;
    }

    private Bitmap setImage(Image image, Context context){
        Bitmap bitmap = null;
        if(image.getType() == CommonCode.TYPE_ALBUM){
            try {
                ParcelFileDescriptor pfd = context.getContentResolver().openFileDescriptor(Uri.parse(image.getPath()), "r");
                bitmap = BitmapFactory.decodeFileDescriptor(pfd.getFileDescriptor());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }else if(image.getType() == CommonCode.TYPE_CAMERA){
            bitmap = BitmapFactory.decodeFile(image.getPath());
        }else if(image.getType() == CommonCode.TYPE_URL){
            UrlAsyncTask urlAsyncTask = new UrlAsyncTask(image.getPath(), context);
            boolean isError = false;
            try {
                bitmap = urlAsyncTask.execute().get(3000, TimeUnit.MILLISECONDS);
            } catch (ExecutionException e) {
                e.printStackTrace();
                isError = true;
            } catch (InterruptedException e) {
                e.printStackTrace();
                isError = true;
            } catch (TimeoutException e) {
                e.printStackTrace();
                isError = true;
            }
            if(isError){
                bitmap = null;
            }
        }
        return bitmap;
    }
}
