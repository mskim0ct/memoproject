package com.example.project_memo;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.PermissionChecker;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project_memo.adapter.ImageCreateRecyclerViewAdapter;
import com.example.project_memo.resource.Image;
import com.example.project_memo.resource.Item;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

//메모 생성, 수정 액티비티
public class CreateItemActivity extends AppCompatActivity implements View.OnClickListener {

    private int CODE;
    private int position;
    private Item preItem;

    private EditText titleEditText;
    private EditText contentEditText;
    private AlertDialog imageSelAlert;

    private RecyclerView imageRecyclerView;
    private ImageCreateRecyclerViewAdapter imageCreateAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        getSupportActionBar().hide();

        titleEditText = findViewById(R.id.title_id);
        contentEditText = findViewById(R.id.content_id);

        imageRecyclerView = findViewById(R.id.image_recycle_view_id);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        imageRecyclerView.setLayoutManager(linearLayoutManager);
        imageCreateAdapter = new ImageCreateRecyclerViewAdapter();
        imageRecyclerView.setAdapter(imageCreateAdapter);

        preItem = null;
        CODE = getIntent().getIntExtra(CommonCode.FLAG, 0);
        if(CODE == CommonCode.CREATE){
            position = -1;
            titleEditText.setText("");
            contentEditText.setText("");
        }else if(CODE == CommonCode.UPDATE){
            preItem = (Item)getIntent().getExtras().getSerializable(CommonCode.DATA);
            position = getIntent().getIntExtra(CommonCode.POSITION, -1);
            titleEditText.setText(preItem.getTitle());
            contentEditText.setText(preItem.getContent());
            if(preItem.getImageArrayList().size() > 0){
                for(Image image : preItem.getImageArrayList()){
                    imageCreateAdapter.addImage(image);
                }
                imageCreateAdapter.notifyDataSetChanged();
            }
        }

        ImageButton imageAddBtn = findViewById(R.id.image_btn_id);
        Button saveBtn = findViewById(R.id.save_btn_id);
        Button cancelBtn = findViewById(R.id.cancel_btn_id);
        imageAddBtn.setOnClickListener(this);
        saveBtn.setOnClickListener(this);
        cancelBtn.setOnClickListener(this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CommonCode.REQUEST_CODE_CAMERA && resultCode == RESULT_OK){
            Bitmap bitmap = (Bitmap)data.getExtras().get("data");

            String fileName = Util.getFormatDate()+".jpg";
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "/memoApp/");
            if(!file.exists()){
                file.mkdir();
            }
            try {
                FileOutputStream out  = new FileOutputStream(file.getAbsolutePath()+fileName);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Image image = new Image();
            image.setPath(file.getAbsolutePath()+fileName);
            image.setType(CommonCode.TYPE_CAMERA);
            imageCreateAdapter.addImage(image);
            imageCreateAdapter.notifyDataSetChanged();
        }else if(requestCode == CommonCode.REQUEST_CODE_ALBUM && data != null){
            String[] projection = {MediaStore.Images.Media._ID};
            Cursor cursor = getContentResolver().query(data.getData(), projection, null, null, null);
            int indx = cursor.getColumnIndex(MediaStore.Images.Media._ID);
            cursor.moveToFirst();
            Uri uri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, cursor.getLong(indx));
            Image image = new Image();
            image.setPath(uri.toString());
            image.setType(CommonCode.TYPE_ALBUM);
            imageCreateAdapter.addImage(image);
            imageCreateAdapter.notifyDataSetChanged();
            cursor.close();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == CommonCode.PERMISSION_WRITE_EXTERNAL_STORAGE_CAMERA){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                openCamera();
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(CreateItemActivity.this);
                builder.setTitle(CreateItemActivity.this.getString(R.string.info));
                builder.setMessage(CreateItemActivity.this.getString(R.string.permission_camera_external_storage));
                AlertDialog alert = builder.create();
                alert.show();
            }
        }else if(requestCode == CommonCode.PERMISSION_READ_EXTERNAL_STORAGE){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                openAlbum();
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(CreateItemActivity.this);
                builder.setTitle(CreateItemActivity.this.getString(R.string.info));
                builder.setMessage(CreateItemActivity.this.getString(R.string.permission_external_storage));
                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private void openAlbum(){
        Intent albumIntent = new Intent(Intent.ACTION_PICK);
        albumIntent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(albumIntent, CommonCode.REQUEST_CODE_ALBUM);
    }

    private void openCamera(){
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CommonCode.REQUEST_CODE_CAMERA);
    }

    private void gotoCamera(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
                openCamera();
            }else{
                String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
                requestPermissions(permissions, CommonCode.PERMISSION_WRITE_EXTERNAL_STORAGE_CAMERA);
            }
        }else{
            int writeExternalPermission = PermissionChecker.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int cameraPermission = PermissionChecker.checkSelfPermission(this, Manifest.permission.CAMERA);
            if(writeExternalPermission == PermissionChecker.PERMISSION_GRANTED
                && cameraPermission == PermissionChecker.PERMISSION_GRANTED){
                openCamera();
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(CreateItemActivity.this);
                builder.setTitle(CreateItemActivity.this.getString(R.string.info));
                builder.setMessage(CreateItemActivity.this.getString(R.string.permission_camera_external_storage));
                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private void gotoAlbum(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                openAlbum();
            }else{
                String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
                requestPermissions(permissions, CommonCode.PERMISSION_READ_EXTERNAL_STORAGE);
            }
        }else{
            int readExternalPermission = PermissionChecker.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
            if(readExternalPermission == PermissionChecker.PERMISSION_GRANTED){
                openAlbum();
            }else{
                AlertDialog.Builder builder = new AlertDialog.Builder(CreateItemActivity.this);
                builder.setTitle(CreateItemActivity.this.getString(R.string.info));
                builder.setMessage(CreateItemActivity.this.getString(R.string.permission_external_storage));
                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private void gotoURL(){
        AlertDialog.Builder urlAlertBuilder = new AlertDialog.Builder(CreateItemActivity.this);
        urlAlertBuilder.setTitle(CreateItemActivity.this.getString(R.string.input_url));
        View view = getLayoutInflater().inflate(R.layout.alertdialog_url, null);
        final EditText editText = view.findViewById(R.id.edit_text_url_id);
        urlAlertBuilder.setView(view);
        urlAlertBuilder.setPositiveButton(CreateItemActivity.this.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Image image = new Image();
                image.setType(CommonCode.TYPE_URL);
                image.setPath(editText.getText().toString());
                imageCreateAdapter.addImageFromURL(image);
                imageCreateAdapter.notifyDataSetChanged();
            }
        });
        AlertDialog urlAlert = urlAlertBuilder.create();
        urlAlert.show();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.camera_id:
                imageSelAlert.dismiss();
                gotoCamera();
                break;
            case R.id.album_id:
                imageSelAlert.dismiss();
                gotoAlbum();
                break;
            case R.id.url_id:
                imageSelAlert.dismiss();
                gotoURL();
                break;
            case R.id.image_btn_id:
                AlertDialog.Builder imageSelAlertBuilder = new AlertDialog.Builder(CreateItemActivity.this);
                View view = getLayoutInflater().inflate(R.layout.listview_selection_image, null);
                imageSelAlertBuilder.setView(view);
                TextView cameraBtn = view.findViewById(R.id.camera_id);
                TextView albumBtn = view.findViewById(R.id.album_id);
                TextView urlBtn = view.findViewById(R.id.url_id);
                cameraBtn.setOnClickListener(this);
                albumBtn.setOnClickListener(this);
                urlBtn.setOnClickListener(this);

                imageSelAlert = imageSelAlertBuilder.create();
                imageSelAlert.show();
                imageSelAlert.getWindow().setLayout(700, WindowManager.LayoutParams.WRAP_CONTENT);
                break;
            case R.id.save_btn_id:
                if(titleEditText.getText().toString().trim().equals("")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(CreateItemActivity.this);
                    builder.setTitle(CreateItemActivity.this.getString(R.string.info));
                    builder.setMessage(CreateItemActivity.this.getString(R.string.input_title));
                    builder.setPositiveButton(CreateItemActivity.this.getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }else{
                    if(CODE == CommonCode.CREATE){
                        Item item = setItem();
                        Intent createIntent = new Intent();
                        createIntent.putExtra(CommonCode.DATA, Util.convertToJson(item));
                        setResult(CommonCode.RESPONSE_CODE_CREATED, createIntent);
                        finish();
                    }else if(CODE == CommonCode.UPDATE){
                        Item item =setItem();
                        Intent updateIntent = new Intent();
                        updateIntent.putExtra(CommonCode.DATA, Util.convertToJson(item));
                        updateIntent.putExtra(CommonCode.POSITION, position);
                        setResult(CommonCode.RESPONSE_CODE_UPDATED, updateIntent);
                        finish();
                    }
                }
                break;
            case R.id.cancel_btn_id:
                finish();
                break;
        }
    }

    private Item setItem(){
        Item item = new Item();
        if(CODE == CommonCode.CREATE){
            item.setID(Util.ID());
        }else{
            item.setID(preItem.getID());
        }
        item.setTitle(titleEditText.getText().toString());
        item.setContent(contentEditText.getText().toString());
        item.setImageArrayList(imageCreateAdapter.getAll());
        item.setRegistrationTime(Util.currentTime());
        return item;
    }
}
