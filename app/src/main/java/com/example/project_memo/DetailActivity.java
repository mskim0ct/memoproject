package com.example.project_memo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project_memo.adapter.ImageRecyclerViewAdapter;
import com.example.project_memo.resource.Image;
import com.example.project_memo.resource.Item;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    private int position;
    private Item item;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getSupportActionBar().hide();

        TextView titleTextView = findViewById(R.id.detail_title_id);
        TextView contentTextView= findViewById(R.id.detail_content_id);

        item = (Item)Util.convertToObject(getIntent().getStringExtra(CommonCode.DATA), Item.class);
        position = getIntent().getIntExtra(CommonCode.POSITION, -1);

        titleTextView.setText(item.getTitle());
        contentTextView.setText(item.getContent());

        RecyclerView imageRecyclerView = findViewById(R.id.detail_recycler_view_id);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        imageRecyclerView.setLayoutManager(linearLayoutManager);
        ImageRecyclerViewAdapter imageAdapter = new ImageRecyclerViewAdapter();
        imageRecyclerView.setAdapter(imageAdapter);
        if(item.getImageArrayList().size() > 0){
            for(Image image : item.getImageArrayList()){
                imageAdapter.addImage(image);
            }
            imageAdapter.notifyDataSetChanged();
        }


        Button deleteBtn = findViewById(R.id.delete_btn_id);
        Button updateBtn = findViewById(R.id.update_btn_id);
        deleteBtn.setOnClickListener(this);
        updateBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.delete_btn_id :
                AlertDialog.Builder builder  = new AlertDialog.Builder(DetailActivity.this);
                builder.setTitle(DetailActivity.this.getString(R.string.delete));
                builder.setMessage(DetailActivity.this.getString(R.string.question_delete_memo));
                builder.setPositiveButton(DetailActivity.this.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent deleteIntent = new Intent();
                        deleteIntent.putExtra(CommonCode.DATA, Util.convertToJson(item));
                        deleteIntent.putExtra(CommonCode.POSITION, position);
                        setResult(CommonCode.RESPONSE_CODE_DELETED, deleteIntent);
                        finish();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton(DetailActivity.this.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog deleteAlert = builder.create();
                deleteAlert.show();
                break;
            case R.id.update_btn_id :
                Intent updateIntent = new Intent();
                updateIntent.setClass(DetailActivity.this, com.example.project_memo.CreateItemActivity.class);
                updateIntent.putExtra(CommonCode.FLAG, CommonCode.UPDATE);
                updateIntent.putExtra(CommonCode.DATA, item);
                updateIntent.putExtra(CommonCode.POSITION, position);
                startActivityForResult(updateIntent, CommonCode.REQUEST_CODE_UPDATE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CommonCode.REQUEST_CODE_UPDATE){
            if(resultCode == CommonCode.RESPONSE_CODE_UPDATED){
                setResult(CommonCode.RESPONSE_CODE_UPDATED, data);
                finish();
            }else{
                finish();
            }
        }
    }
}
