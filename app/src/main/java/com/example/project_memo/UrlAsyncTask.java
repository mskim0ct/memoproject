package com.example.project_memo;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class UrlAsyncTask extends AsyncTask<Void, Void, Bitmap> {
    private String path;
    private boolean isError;
    private boolean isUrlError;
    private Context context;
    public UrlAsyncTask(String path, Context context){
        this.path = path;
        isError = false;
        isUrlError = false;
        this.context = context;
    }

    @Override
    protected Bitmap doInBackground(Void... voids) {
        Bitmap bitmap = null;
        try {
            URL url = new URL(path);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            InputStream is = conn.getInputStream();
            bitmap = BitmapFactory.decodeStream(is);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            isUrlError = true;
            isError = true;
        } catch (IOException e) {
            e.printStackTrace();
            isError = true;
        }
        if(isError){
            return null;
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        Log.d("UrlAsyncTask", "onPostExecute 호출");
        if(isUrlError){
            Log.d("UrlAsyncTask", "isUrlError == true");
            createError();
        }
    }

    protected void createError() {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.info))
                .setMessage(context.getString(R.string.confirm_url))
                .create();
            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    dialogInterface.dismiss();
                }
            });

        dialog.show();
    }
}
