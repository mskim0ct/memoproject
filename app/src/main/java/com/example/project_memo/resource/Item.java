package com.example.project_memo.resource;

import java.io.Serializable;
import java.util.ArrayList;

public class Item implements  Serializable {

    private String ID;
    private String title;
    private String content;
    private long registrationTime;
    private ArrayList<Image> imageArrayList;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getRegistrationTime() {
        return registrationTime;
    }

    public void setRegistrationTime(long registrationTime) {
        this.registrationTime = registrationTime;
    }

    public ArrayList<Image> getImageArrayList() {
        return imageArrayList;
    }

    public void setImageArrayList(ArrayList<Image> imageArrayList) {
        this.imageArrayList = imageArrayList;
    }

    @Override
    public String toString() {
        return "Item{" +
                "ID='" + ID + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", registrationTime=" + registrationTime +
                ", imageArrayList=" + imageArrayList +
                '}';
    }
}
