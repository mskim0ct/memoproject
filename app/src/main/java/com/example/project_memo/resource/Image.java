package com.example.project_memo.resource;

import java.io.Serializable;

public class Image implements Serializable {

    private int type;
    private String path;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "Image{" +
                "type=" + type +
                ", path='" + path + '\'' +
                '}';
    }
}
