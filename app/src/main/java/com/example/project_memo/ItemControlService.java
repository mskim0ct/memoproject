package com.example.project_memo;

import android.content.Context;

import com.example.project_memo.adapter.InternalStorageFileAdapter;
import com.example.project_memo.adapter.ItemListViewAdapter;
import com.example.project_memo.resource.Item;

import java.util.ArrayList;

//mainlistview 부분 업데이트 및 내부저장소 파일에 저장
public class ItemControlService {

    ItemListViewAdapter itemListViewAdapter;
    InternalStorageFileAdapter fileAdapter;

    public ItemControlService(Context context){
        itemListViewAdapter = new ItemListViewAdapter();
        fileAdapter = new InternalStorageFileAdapter(context);
    }

    public ItemListViewAdapter getItemListViewAdapter(){
        return itemListViewAdapter;
    }

    public void loadItem(){
        ArrayList<Item> itemArrayList = fileAdapter.getAll();
        if(itemArrayList.size() > 0){
            for(Item item : itemArrayList){
                itemListViewAdapter.add(item);
            }
        }
    }

    public void addItem(Item item){
        fileAdapter.createFile(item.getID(), item);
        itemListViewAdapter.add(item);
        itemListViewAdapter.notifyDataSetChanged();
    }

    public void updateItem(Item item, int position){
        fileAdapter.updateFile(item.getID(), item);
        itemListViewAdapter.update(item, position);
        itemListViewAdapter.notifyDataSetChanged();
    }

    public void deleteItem(Item item, int position){
        fileAdapter.deleteFile(item.getID());
        itemListViewAdapter.delete(position);
        itemListViewAdapter.notifyDataSetChanged();
    }

    public Item retrieveItem(Item item){
        return fileAdapter.get(item.getID());
    }
}
