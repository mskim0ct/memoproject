package com.example.project_memo;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public class Util {

    public static String ID(){
        return UUID.randomUUID().toString();
    }

    public static String getFormatDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
        return sdf.format(new Date(System.currentTimeMillis()));
    }

    public static long currentTime(){
        return System.currentTimeMillis();
    }

    public static String convertToJson(Object obj){
        Gson gson = new Gson();
        return gson.toJson(obj);
    }

    public static Object convertToObject(String json, Class cls ){
        Gson gson = new Gson();
        return gson.fromJson(json, cls);
    }
}
