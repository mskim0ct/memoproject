package com.example.project_memo;

public class CommonCode {

    public static String FLAG = "FLAG";
    public static int CREATE = 1;
    public static int UPDATE = 2;
    public static int DETAIL = 3;
    public static String DATA = "DATA";
    public static String POSITION = "POSITION";

    public static int REQUEST_CODE_CREATE=10;
    public static int REQUEST_CODE_DETAIL=11;
    public static int REQUEST_CODE_UPDATE=12;

    public static int RESPONSE_CODE_CREATED=100;
    public static int RESPONSE_CODE_UPDATED=101;
    public static int RESPONSE_CODE_DELETED=102;

    public static int REQUEST_CODE_CAMERA = 1001;
    public static int REQUEST_CODE_ALBUM = 1002;
    public static int REQUEST_CODE_URL = 1003;

    public static int TYPE_CAMERA = 1011;
    public static int TYPE_ALBUM = 1012;
    public static int TYPE_URL = 1013;

    public static int PERMISSION_READ_EXTERNAL_STORAGE=2000;
    public static int PERMISSION_WRITE_EXTERNAL_STORAGE_CAMERA=2001;

}
