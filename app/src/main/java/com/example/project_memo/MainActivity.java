package com.example.project_memo;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.project_memo.resource.Item;

public class MainActivity extends AppCompatActivity {
    private String TAG = "MainActivity";

    ItemControlService itemControlService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        itemControlService = new ItemControlService(MainActivity.this);
        //어댑터 설정
        ListView mainListView = findViewById(R.id.main_listview_id);
        View footer = getLayoutInflater().inflate(R.layout.listview_main_footer, null, false);
        mainListView.addFooterView(footer, null, false);
        itemControlService.loadItem();
        mainListView.setAdapter(itemControlService.getItemListViewAdapter());

        //리스트뷰 메모 선택시
        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailIntent = new Intent();
                detailIntent.setClass(MainActivity.this, com.example.project_memo.DetailActivity.class);
                detailIntent.putExtra(CommonCode.FLAG, CommonCode.DETAIL);
                detailIntent.putExtra(CommonCode.POSITION, position);
                Log.d(TAG, Util.convertToJson(parent.getItemAtPosition(position)));
                detailIntent.putExtra(CommonCode.DATA, Util.convertToJson(parent.getItemAtPosition(position)));
                startActivityForResult(detailIntent, CommonCode.REQUEST_CODE_DETAIL);
            }
        });

        //버튼추가
        ImageButton addBtn = findViewById(R.id.add_btn_id);
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent createIntent = new Intent();
                createIntent.setClass(MainActivity.this, com.example.project_memo.CreateItemActivity.class);
                createIntent.putExtra(CommonCode.FLAG, CommonCode.CREATE);
                startActivityForResult(createIntent, CommonCode.REQUEST_CODE_CREATE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CommonCode.REQUEST_CODE_DETAIL){
            if(resultCode == CommonCode.RESPONSE_CODE_DELETED){
                Item item = (Item)Util.convertToObject(data.getStringExtra(CommonCode.DATA), Item.class);
                int position = data.getIntExtra(CommonCode.POSITION, -1);
                itemControlService.deleteItem(item, position);
            }else if(resultCode == CommonCode.RESPONSE_CODE_UPDATED){
                Item item = (Item)Util.convertToObject(data.getStringExtra(CommonCode.DATA), Item.class);
                int position = data.getIntExtra(CommonCode.POSITION, -1);
                itemControlService.updateItem(item, position);
            }
        }else if(requestCode == CommonCode.REQUEST_CODE_CREATE){
            if(resultCode == CommonCode.RESPONSE_CODE_CREATED){
                Item item = (Item)Util.convertToObject(data.getStringExtra(CommonCode.DATA), Item.class);
                itemControlService.addItem(item);
            }
        }
    }
}
