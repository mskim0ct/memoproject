### 이미지를 포함하는 메모앱
- 이미지는 카메라, 앨범, url을 이용하여 추가

---------------------

### 화면

<br>

![main_activity](./문서/메인화면.JPG)[메인화면]

<br>

![delete_activity](./문서/삭제화면.JPG)[삭제화면]

<br>

![add_activity](./문서/추가화면.JPG)[추가화면]

<br>

![add_image](./문서/이미지선택리스트.JPG)[이미지선택리스트]


<br>

![retrieve_activity](./문서/조회화면.JPG)[조회화면]

<br>

![update_activity](./문서/수정화면.JPG)[수정화면]

<br>

-----------------

### 클래스 관계

![class](./문서/클래스구조.png)

<br>

- MainActivity : 메인화면
- DetailActivity : 저장한 메모 조회 화면 / 삭제, 수정 기능 포함
- CreateItemActivity : 메모 생성/수정 화면 / 취소, 생성 기능 포함
- UrlAsyncTask : url을 이용한 이미지를 가져오는 클래스
- ItemControlService : 메모에 대한 저장/수정/삭제/조회 기능을 포함하는 클래스
- InternalStorageFileAdapter : 파일 형태로 각각의 메모를 관리하는 클래스
- Util/CommonCode : 공통적으로 사용하는 기능, 상수를 포함하는 클래스
- Image/Item : 데이터 타입
- ItemListViewAdapter : 메인화면의 리스트뷰 어댑터
- ImageRecyclerViewAdapter : 메모 조회 화면의 RecyclerView 어댑터
- ImageCreateRecyclerViewAdapter : 메모 생성 화면의 RecyclerView 어댑터
